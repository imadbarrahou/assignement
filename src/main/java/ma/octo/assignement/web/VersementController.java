package ma.octo.assignement.web;

import java.math.BigDecimal;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.util.CollectionUtils;
import org.springframework.web.bind.annotation.*;

import ma.octo.assignement.domain.Compte;
import ma.octo.assignement.domain.Versement;
import ma.octo.assignement.dto.VersementDTO;
import ma.octo.assignement.exceptions.CompteNonExistantException;
import ma.octo.assignement.exceptions.SoldeDisponibleInsuffisantException;
import ma.octo.assignement.exceptions.TransactionException;
import ma.octo.assignement.repository.CompteRepository;
import ma.octo.assignement.repository.UtilisateurRepository;
import ma.octo.assignement.repository.VersementRepository;
import ma.octo.assignement.service.AutiService;

@RestController
@RequestMapping("/versements")
public class VersementController {

	    @Autowired
	    private VersementRepository versementrepo;
	    @Autowired
	    private AutiService monservice;
	    @Autowired
	    private UtilisateurRepository utilisateurrep;
	    @Autowired
	    private CompteRepository compterep;
	   
	    
	    @GetMapping("lister_versements")
	    List<Versement> loadAll() {
	        List<Versement> all = versementrepo.findAll();

	        if (CollectionUtils.isEmpty(all)) {
	            return null;
	        } else {
	            return all;
	        }
	    }
	    
	    
	    
	    @PostMapping("/executerVersements")

	    public void createTransaction(@RequestBody VersementDTO versementDTO)
	            throws SoldeDisponibleInsuffisantException, CompteNonExistantException, TransactionException {
	         System.out.println(versementDTO.getNrCompteBeneficiaire());
	        Compte cB = compterep.findByNrCompte(versementDTO.getNrCompteBeneficiaire());
	        if (cB == null) {
	            System.out.println("Compte Non existant");
	            throw new CompteNonExistantException("Compte Non existant");
	        }

	        if (versementDTO.getMontantVersement().equals(null)) {
	            System.out.println("Montant vide");
	            throw new TransactionException("Montant vide");
	        } else if (versementDTO.getMontantVersement().intValue() == 0) {
	            System.out.println("Montant vide");
	            throw new TransactionException("Montant vide");
	        } else if (versementDTO.getMontantVersement().intValue() < 10) {
	            System.out.println("Montant minimal de Versement non atteint");
	            throw new TransactionException("Montant minimal de Versement non atteint");
	        }

	        if (versementDTO.getMotif().length() < 0) {
	            System.out.println("Motif vide");
	            throw new TransactionException("Motif vide");
	        }

	   
	        cB.setSolde(new BigDecimal(cB.getSolde().intValue() + versementDTO.getMontantVersement().intValue()));
	        compterep.save(cB);

	        Versement versement = new Versement();
	        versement.setDateExecution(versementDTO.getDate());
	        versement.setCompteBeneficiaire(cB);
	        versement.setNom_prenom_emetteur(versementDTO.getNom_prenom_emetteur());
	        versement.setMontantVersement(versementDTO.getMontantVersement());
	        versement.setMotifVersement(versementDTO.getMotif());

	        
	        versementrepo.save(versement);

	        monservice.auditVersement("Versement de la part de  " + versementDTO.getNom_prenom_emetteur() + " vers " + versementDTO
	                        .getNrCompteBeneficiaire() + " d'un montant de " + versementDTO.getMontantVersement()
	                        .toString());
	    }

	    
	    
	    
}
